<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loker extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		admin_logged_in();
		$this->load->model('model_loker');
		$this->load->helper(array('form','url'));
	}
	
	public function index()
	{
		
		$isi['content'] 	= 'backend/loker/tampil_loker';
		$isi['judul'] 		= 'Master';
		$isi['sub_judul'] 	= 'loker';
		$isi['data']		= $this->db->get('loker');

		$this->load->view('backend/tampil_home', $isi);
	}

	public function tambah()
	{
		
		$isi['content'] 	= 'backend/loker/form_tambahloker';
		$isi['judul'] 		= 'Master';
		$isi['sub_judul'] 	= 'Tambah loker';
		
		$isi['id_loker'] 		= '';
		$isi['judul_loker'] 	= '';
		$isi['penulis'] 		= '';
		$isi['isi'] 			= '';
		$isi['tanggal'] 		= '';
		$isi['gambar'] 			= '';

		$this->load->view('backend/tampil_home', $isi);
	}

	public function simpan()
	{
		$config['upload_path'] = './uploads/loker/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size']	= '1000'; //KB
		$config['max_width']  = '2000'; //pixels
		$config['max_height']  = '2000'; //pixels
		
		$this->upload->initialize($config);

		if(!$this->upload->do_upload('gambar')){
			$data['id_loker'] 		= $this->input->post('id_loker');
			$data['judul_loker'] 	= $this->input->post('judul_loker');
			$data['penulis'] 		= $this->input->post('penulis');
			$data['isi'] 			= $this->input->post('isi');
			$data['tanggal'] 		= $this->input->post('tanggal');
			
		} else {

			$img=$this->upload->data();
			
			$data['id_loker'] 		= $this->input->post('id_loker');
			$data['judul_loker'] 	= $this->input->post('judul_loker');
			$data['penulis'] 		= $this->input->post('penulis');
			$data['isi'] 			= $this->input->post('isi');
			$data['tanggal'] 		= $this->input->post('tanggal');
			$data['gambar'] 		= $img['file_name'];
		}

		$key= $this->input->post('id_loker');
		
		$query = $this->model_loker->getdata($key);
		if($query->num_rows() > 0)
		{
			
			$this->model_loker->getupdate($key,$data);
			$this->session->set_flashdata('Info','Data berhasil di update');
		}else{
			$this->model_loker->getinsert($data);
			$this->session->set_flashdata('Info','Data berhasil di simpan');
		}
		
		redirect('admin/loker/index');
	}

	public function edit()
	{
		
		$isi['content'] 	= 'backend/loker/form_tambahloker';
		$isi['judul'] 		= 'Master';
		$isi['sub_judul'] 	= 'Edit Lowongan Kerja';

		$key = $this->uri->segment(4);
		$this->db->where('id_loker',$key);
		$query = $this->db->get('loker');
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $row) 
			{
				$isi['id_loker'] 		= $row->id_loker;
				$isi['judul_loker'] 	= $row->judul_loker;
				$isi['penulis'] 		= $row->penulis;
				$isi['isi'] 			= $row->isi;
				$isi['tanggal'] 		= $row->tanggal;
				$isi['gambar'] 			= $row->gambar;
			}
		}
	else{
		$isi['id_loker'] 		= '';
		$isi['judul_loker'] 	= '';
		$isi['penulis'] 		= '';
		$isi['isi'] 			= '';
		$isi['tanggal'] 		= '';
		$isi['gambar'] 			= '';
	}
	$this->load->view('backend/tampil_home', $isi);
}

public function delete()
{

	$key = $this->uri->segment(4);
	$this->db->where('id_loker',$key);
	$query = $this->db->get('loker');
	if($query->num_rows()>0)
	{
		$this->model_loker->getdelete($key);
	}
	redirect('admin/loker/index');
}

}
?>