<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_loker extends CI_model {

	public function getdata($key)
	{
		$this->db->where('id_loker', $key);
		$hasil = $this->db->get('loker');
		return $hasil;
	}

	function getLoker($where = ''){
		return $this->db->query("select * from loker $where;");
	}

	public function getinsert($data)
	{
		$this->db->insert('loker',$data);
	}

	public function getupdate($key,$data)
	{
		$this->db->where('id_loker',$key);
		$this->db->update('loker',$data);
	}

	public function getdelete($key)
	{
		$this->db->where('id_loker',$key)
		->delete('loker');
	}

	public function paging($number,$offset){
		return $query = $this->db->get('loker',$number,$offset)->result_array();
	}

	public function jumlah_data(){
		return $this->db->get('loker')->num_rows();
	}

	public function klik($key){
		return $this->db->query("UPDATE loker SET status = status + 1 WHERE id_loker ='$key' ");
	}

}
?>