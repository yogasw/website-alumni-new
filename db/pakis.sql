/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : pakis

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2019-05-25 15:04:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` text NOT NULL,
  `telp` varchar(13) NOT NULL,
  `foto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('2', 'Admin', 'Admin', '21232f297a57a5a743894a0e4a801fc3', '0895368200241', 'profile-pic.jpg');

-- ----------------------------
-- Table structure for agenda
-- ----------------------------
DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id_agenda` int(5) NOT NULL AUTO_INCREMENT,
  `judul_agenda` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  `gambar` text NOT NULL,
  PRIMARY KEY (`id_agenda`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agenda
-- ----------------------------
INSERT INTO `agenda` VALUES ('8', 'Perayaan Tahun Baru 2018', '<p>Isi Perayaan Tahun Baru 2018</p>', '2018-01-01 10:00:00', '0', 'Tulips.jpg');

-- ----------------------------
-- Table structure for album
-- ----------------------------
DROP TABLE IF EXISTS `album`;
CREATE TABLE `album` (
  `id_album` int(11) NOT NULL AUTO_INCREMENT,
  `nama_album` varchar(100) NOT NULL,
  PRIMARY KEY (`id_album`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of album
-- ----------------------------
INSERT INTO `album` VALUES ('1', 'Undangan');
INSERT INTO `album` VALUES ('4', 'Reuni');

-- ----------------------------
-- Table structure for alumni
-- ----------------------------
DROP TABLE IF EXISTS `alumni`;
CREATE TABLE `alumni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nisn` varchar(20) NOT NULL,
  `password` text,
  `nama_lengkap` varchar(30) DEFAULT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `telp` varchar(13) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tahun_lulus` int(4) DEFAULT NULL,
  `id_jurusan` int(5) DEFAULT NULL,
  `id_kabupaten` int(4) DEFAULT NULL,
  `id_provinsi` int(4) DEFAULT NULL,
  `agama` enum('Islam','Kristen','Katholik','Hindu','Budha') DEFAULT NULL,
  `profesi` varchar(30) DEFAULT NULL,
  `alamat` text,
  `foto` text,
  PRIMARY KEY (`id`),
  KEY `id_jurusan` (`id_jurusan`),
  KEY `id_kabupaten` (`id_kabupaten`),
  KEY `id_provinsi` (`id_provinsi`),
  CONSTRAINT `alumni_ibfk_1` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `alumni_ibfk_2` FOREIGN KEY (`id_kabupaten`) REFERENCES `kabupaten` (`id_kabupaten`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of alumni
-- ----------------------------
INSERT INTO `alumni` VALUES ('2', '9950012401', 'fe01ce2a7fbac8fafaed7c982a04e229', 'Bayu Nugroho', 'Laki-laki', 'BayuNugroho@gmail.com', '654321', 'Solo', '1992-06-10', '2010', '2', '3372', '33', 'Kristen', 'Atlet', 'Solo', 'Model-1.jpg');
INSERT INTO `alumni` VALUES ('3', '9950012402', 'fe01ce2a7fbac8fafaed7c982a04e229', 'Ratu Tisha', 'Perempuan', 'RatuTisha@gmail.com', '7890', 'Bandung', '1989-02-09', '2007', '3', '3173', '31', 'Islam', 'Sekjen PSSO', 'Jakarta', '18252529_321597508257501_2411866905435439104_n.jpg');
INSERT INTO `alumni` VALUES ('4', '9950012403', 'fe01ce2a7fbac8fafaed7c982a04e229', 'yog', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('5', '9950012404', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('6', '9950012405', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('7', '9950012406', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('8', '9950012407', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('9', '9950012408', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('10', '9950012409', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('11', '9950012410', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('12', '9950012411', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('13', '9950012412', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('14', '9950012413', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('15', '9950012414', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('16', '9950012415', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('17', '9950012416', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('18', '9950012417', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('19', '9950012418', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `alumni` VALUES ('20', '9950012419', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for berita
-- ----------------------------
DROP TABLE IF EXISTS `berita`;
CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `judul_berita` varchar(100) NOT NULL,
  `penulis` varchar(25) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(11) NOT NULL,
  `gambar` text NOT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of berita
-- ----------------------------
INSERT INTO `berita` VALUES ('7', 'Apel Pagi Bersama Bupati Klaten Di SMAN1 Wonosari Klaten', 'Admin', '<p>Kamis (4/3) SMAN 1 Wonosari Klaten kedatangan tanu istimewa. Bapak Bupati Klaten berkenan hadir untuk melakukan apel pagi di SMA tercinta ini. Di mulai sekitar jam 08.00 Wib Bapak Sunarno memimpin apel yang diikuti oleh seluruh siswa SMAN 1 Wonosari beserta guru dan karyawan.</p>\r\n<p>Dalam sambutanya, Bapak Sunarno menekankan kepada siswa untuk selalu berkomitmen dengan apa yang menjadi kewajibanya. &rdquo; sebagai seorang siswa harus menjalankan apa yang menjadi kewajiban. seperti belajar yang giat&rdquo; jelas Bapak Sunarno.</p>\r\n<p>Sebelum acara berakhir, Bapak Sunarno berkesempatan memberikan kenang-kenangan kepada SMAN 1 Wonosari dan penyematan PIN.&nbsp; aSetelah itu acara di akhiri dengan acara jabat tangan seluruh siswa kepada Bapak Sunarno.</p>', '2017-12-25', '0', 'Koala.jpg');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id_config` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id_config`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', 'logo header', 'banner-smansari.png');
INSERT INTO `config` VALUES ('2', 'title_home', 'Air Sugihan');
INSERT INTO `config` VALUES ('3', 'sambutan', '<p><em>Assalam mu’alaikum wr. wb.</em></p>\r\n<p>Puji syukur rahmat dan karunia Allah SWT sehingga kami dapat menerbitkan website resmi Alumni SMA Negeri 1 Wonosari Klaten sebagai sarana informasi dan komunikasi alumni.</p>\r\n<p>Website ini merupakan sarana bagi para alumni SMA Negeri 1 Wonosari untuk saling berkomunikasi dan mendapatkan informasi-informasi mengenai berita dan agenda tentang alumni. Website ini juga dapat digunakan oleh pihak masyarakat umum untuk mendapat informasi di website ini. Bagi alumni SMA N 1 Wonosari diwajibkan untuk melakukan pendaftaran sebelum dapat memanfaatkan fasilitas-fasilitas yang tersedia.<br><br>Kami berharap, dengan adanya media layanan informasi situs ini semoga dapat mewujudkan hubungan silaturahmi yang lebih erat lagi, menambah wawasan, mempermudah dan mempercepat proses dalam mendapatkan info yang dibutuhkan.</p>\r\n<p><em>Wassalam mu’alaikum wr. wb.</em></p>');
INSERT INTO `config` VALUES ('4', 'kepsek', '<p>Kepala Sekolah SMA NEGERI 1 WONOSARI KLATEN</p>');
INSERT INTO `config` VALUES ('5', 'foto_kepsek', 'profile-pic.jpg');
INSERT INTO `config` VALUES ('6', 'teks_kontak', '<p>Telp. (0272) 551584 <br>Email : sman1@gmail.com</p>');
INSERT INTO `config` VALUES ('7', 'Visi', '<p>Alumni adalah bagian integral dari komunitas besar kami yang memiliki hubungan dengan SMA Negeri 1 Wonosari Klaten. Alumni adalah merupakan orang-orang yang memberikan kontribusi signifikan terhadap SMA Negeri 1 Wonosari Klaten. </p>');
INSERT INTO `config` VALUES ('8', 'footer', '<p>© 2017 SMANSARI - All Rights Reserved.</p>');
INSERT INTO `config` VALUES ('9', 'Misi', '<p>Kami ingin memelihara hubungan antara SMA Negeri 1 Wonosari Klaten dengan alumni yang tinggal di Indonesia dan di luar negeri. Kami menyediakan alat bagi alumni agar tetap terhubung dengan teman-teman semasa bersekolah di SMA Negeri 1 Wonosari Klaten dan bahkan menemukan teman-teman baru dengan kesamaan minat dan tujuan melalui kegiatan alumni lain yang diadakan di Klaten dan sekitarnya. Dengan tetap terhubung, kami menyambut ide-ide Anda untuk meningkatkan program kami agar relevan bagi alumni dan siswa SMA Negeri 1 Wonosari Klaten di masa depan. Selain itu kami juga berharap dapatmembuat Anda tetap terhubung dengan informasi tentang perkembangan yang terjadi di SMA Negeri 1 Wonosari Klaten.</p>');
INSERT INTO `config` VALUES ('10', 'slider1', '12.jpg');
INSERT INTO `config` VALUES ('11', 'slider2', '21.jpg');

-- ----------------------------
-- Table structure for galeri
-- ----------------------------
DROP TABLE IF EXISTS `galeri`;
CREATE TABLE `galeri` (
  `id_galeri` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `gambar` text NOT NULL,
  `id_album` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeri`),
  KEY `id_album` (`id_album`),
  CONSTRAINT `galeri_ibfk_1` FOREIGN KEY (`id_album`) REFERENCES `album` (`id_album`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of galeri
-- ----------------------------
INSERT INTO `galeri` VALUES ('75', '18160793_1969646479924917_2833989665814478848_n1.j', '18160793_1969646479924917_2833989665814478848_n1.jpg', '1');
INSERT INTO `galeri` VALUES ('76', '18160492_1855144614808738_7066450120912404480_n1.j', '18160492_1855144614808738_7066450120912404480_n1.jpg', '1');
INSERT INTO `galeri` VALUES ('77', '14063588_287379445053498_236549236542406656_n.jpg', '14063588_287379445053498_236549236542406656_n.jpg', '4');
INSERT INTO `galeri` VALUES ('78', '17934334_1430767720312595_499883327956713472_n.jpg', '17934334_1430767720312595_499883327956713472_n.jpg', '4');

-- ----------------------------
-- Table structure for jurusan
-- ----------------------------
DROP TABLE IF EXISTS `jurusan`;
CREATE TABLE `jurusan` (
  `id_jurusan` int(5) NOT NULL AUTO_INCREMENT,
  `nama_jurusan` varchar(25) NOT NULL,
  `singkatan` varchar(25) NOT NULL,
  PRIMARY KEY (`id_jurusan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jurusan
-- ----------------------------
INSERT INTO `jurusan` VALUES ('2', 'Ilmu Pengetahuan Sosial', 'IPS');
INSERT INTO `jurusan` VALUES ('3', 'Bahasa', 'BHS1');

-- ----------------------------
-- Table structure for kabupaten
-- ----------------------------
DROP TABLE IF EXISTS `kabupaten`;
CREATE TABLE `kabupaten` (
  `id_kabupaten` int(4) NOT NULL,
  `id_provinsi` int(4) NOT NULL,
  `nama_kabupaten` varchar(30) NOT NULL,
  PRIMARY KEY (`id_kabupaten`),
  KEY `id_provinsi` (`id_provinsi`),
  CONSTRAINT `kabupaten_ibfk_1` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kabupaten
-- ----------------------------
INSERT INTO `kabupaten` VALUES ('1101', '11', 'Kab. Simeulue');
INSERT INTO `kabupaten` VALUES ('1102', '11', 'Kab. Aceh Singkil');
INSERT INTO `kabupaten` VALUES ('1103', '11', 'Kab. Aceh Selatan');
INSERT INTO `kabupaten` VALUES ('1104', '11', 'Kab. Aceh Tenggara');
INSERT INTO `kabupaten` VALUES ('1105', '11', 'Kab. Aceh Timur');
INSERT INTO `kabupaten` VALUES ('1106', '11', 'Kab. Aceh Tengah');
INSERT INTO `kabupaten` VALUES ('1107', '11', 'Kab. Aceh Barat');
INSERT INTO `kabupaten` VALUES ('1108', '11', 'Kab. Aceh Besar');
INSERT INTO `kabupaten` VALUES ('1109', '11', 'Kab. Pidie');
INSERT INTO `kabupaten` VALUES ('1110', '11', 'Kab. Bireuen');
INSERT INTO `kabupaten` VALUES ('1111', '11', 'Kab. Aceh Utara');
INSERT INTO `kabupaten` VALUES ('1112', '11', 'Kab. Aceh Barat Daya');
INSERT INTO `kabupaten` VALUES ('1113', '11', 'Kab. Gayo Lues');
INSERT INTO `kabupaten` VALUES ('1114', '11', 'Kab. Aceh Tamiang');
INSERT INTO `kabupaten` VALUES ('1115', '11', 'Kab. Nagan Raya');
INSERT INTO `kabupaten` VALUES ('1116', '11', 'Kab. Aceh Jaya');
INSERT INTO `kabupaten` VALUES ('1117', '11', 'Kab. Bener Meriah');
INSERT INTO `kabupaten` VALUES ('1118', '11', 'Kab. Pidie Jaya');
INSERT INTO `kabupaten` VALUES ('1171', '11', 'Kota Banda Aceh');
INSERT INTO `kabupaten` VALUES ('1172', '11', 'Kota Sabang');
INSERT INTO `kabupaten` VALUES ('1173', '11', 'Kota Langsa');
INSERT INTO `kabupaten` VALUES ('1174', '11', 'Kota Lhokseumawe');
INSERT INTO `kabupaten` VALUES ('1175', '11', 'Kota Subulussalam');
INSERT INTO `kabupaten` VALUES ('1201', '12', 'Kab. Nias');
INSERT INTO `kabupaten` VALUES ('1202', '12', 'Kab. Mandailing Natal');
INSERT INTO `kabupaten` VALUES ('1203', '12', 'Kab. Tapanuli Selatan');
INSERT INTO `kabupaten` VALUES ('1204', '12', 'Kab. Tapanuli Tengah');
INSERT INTO `kabupaten` VALUES ('1205', '12', 'Kab. Tapanuli Utara');
INSERT INTO `kabupaten` VALUES ('1206', '12', 'Kab. Toba Samosir');
INSERT INTO `kabupaten` VALUES ('1207', '12', 'Kab. Labuhan Batu');
INSERT INTO `kabupaten` VALUES ('1208', '12', 'Kab. Asahan');
INSERT INTO `kabupaten` VALUES ('1209', '12', 'Kab. Simalungun');
INSERT INTO `kabupaten` VALUES ('1210', '12', 'Kab. Dairi');
INSERT INTO `kabupaten` VALUES ('1211', '12', 'Kab. Karo');
INSERT INTO `kabupaten` VALUES ('1212', '12', 'Kab. Deli Serdang');
INSERT INTO `kabupaten` VALUES ('1213', '12', 'Kab. Langkat');
INSERT INTO `kabupaten` VALUES ('1214', '12', 'Kab. Nias Selatan');
INSERT INTO `kabupaten` VALUES ('1215', '12', 'Kab. Humbang Hasundutan');
INSERT INTO `kabupaten` VALUES ('1216', '12', 'Kab. Pakpak Bharat');
INSERT INTO `kabupaten` VALUES ('1217', '12', 'Kab. Samosir');
INSERT INTO `kabupaten` VALUES ('1218', '12', 'Kab. Serdang Bedagai');
INSERT INTO `kabupaten` VALUES ('1219', '12', 'Kab. Batu Bara');
INSERT INTO `kabupaten` VALUES ('1220', '12', 'Kab. Padang Lawas Utara');
INSERT INTO `kabupaten` VALUES ('1221', '12', 'Kab. Padang Lawas');
INSERT INTO `kabupaten` VALUES ('1222', '12', 'Kab. Labuhan Batu Selatan');
INSERT INTO `kabupaten` VALUES ('1223', '12', 'Kab. Labuhan Batu Utara');
INSERT INTO `kabupaten` VALUES ('1224', '12', 'Kab. Nias Utara');
INSERT INTO `kabupaten` VALUES ('1225', '12', 'Kab. Nias Barat');
INSERT INTO `kabupaten` VALUES ('1271', '12', 'Kota Sibolga');
INSERT INTO `kabupaten` VALUES ('1272', '12', 'Kota Tanjung Balai');
INSERT INTO `kabupaten` VALUES ('1273', '12', 'Kota Pematang Siantar');
INSERT INTO `kabupaten` VALUES ('1274', '12', 'Kota Tebing Tinggi');
INSERT INTO `kabupaten` VALUES ('1275', '12', 'Kota Medan');
INSERT INTO `kabupaten` VALUES ('1276', '12', 'Kota Binjai');
INSERT INTO `kabupaten` VALUES ('1277', '12', 'Kota Padangsidimpuan');
INSERT INTO `kabupaten` VALUES ('1278', '12', 'Kota Gunungsitoli');
INSERT INTO `kabupaten` VALUES ('1301', '13', 'Kab. Kepulauan Mentawai');
INSERT INTO `kabupaten` VALUES ('1302', '13', 'Kab. Pesisir Selatan');
INSERT INTO `kabupaten` VALUES ('1303', '13', 'Kab. Solok');
INSERT INTO `kabupaten` VALUES ('1304', '13', 'Kab. Sijunjung');
INSERT INTO `kabupaten` VALUES ('1305', '13', 'Kab. Tanah Datar');
INSERT INTO `kabupaten` VALUES ('1306', '13', 'Kab. Padang Pariaman');
INSERT INTO `kabupaten` VALUES ('1307', '13', 'Kab. Agam');
INSERT INTO `kabupaten` VALUES ('1308', '13', 'Kab. Lima Puluh Kota');
INSERT INTO `kabupaten` VALUES ('1309', '13', 'Kab. Pasaman');
INSERT INTO `kabupaten` VALUES ('1310', '13', 'Kab. Solok Selatan');
INSERT INTO `kabupaten` VALUES ('1311', '13', 'Kab. Dharmasraya');
INSERT INTO `kabupaten` VALUES ('1312', '13', 'Kab. Pasaman Barat');
INSERT INTO `kabupaten` VALUES ('1371', '13', 'Kota Padang');
INSERT INTO `kabupaten` VALUES ('1372', '13', 'Kota Solok');
INSERT INTO `kabupaten` VALUES ('1373', '13', 'Kota Sawah Lunto');
INSERT INTO `kabupaten` VALUES ('1374', '13', 'Kota Padang Panjang');
INSERT INTO `kabupaten` VALUES ('1375', '13', 'Kota Bukittinggi');
INSERT INTO `kabupaten` VALUES ('1376', '13', 'Kota Payakumbuh');
INSERT INTO `kabupaten` VALUES ('1377', '13', 'Kota Pariaman');
INSERT INTO `kabupaten` VALUES ('1401', '14', 'Kab. Kuantan Singingi');
INSERT INTO `kabupaten` VALUES ('1402', '14', 'Kab. Indragiri Hulu');
INSERT INTO `kabupaten` VALUES ('1403', '14', 'Kab. Indragiri Hilir');
INSERT INTO `kabupaten` VALUES ('1404', '14', 'Kab. Pelalawan');
INSERT INTO `kabupaten` VALUES ('1405', '14', 'Kab. S I A K');
INSERT INTO `kabupaten` VALUES ('1406', '14', 'Kab. Kampar');
INSERT INTO `kabupaten` VALUES ('1407', '14', 'Kab. Rokan Hulu');
INSERT INTO `kabupaten` VALUES ('1408', '14', 'Kab. Bengkalis');
INSERT INTO `kabupaten` VALUES ('1409', '14', 'Kab. Rokan Hilir');
INSERT INTO `kabupaten` VALUES ('1410', '14', 'Kab. Kepulauan Meranti');
INSERT INTO `kabupaten` VALUES ('1471', '14', 'Kota Pekanbaru');
INSERT INTO `kabupaten` VALUES ('1473', '14', 'Kota D U M A I');
INSERT INTO `kabupaten` VALUES ('1501', '15', 'Kab. Kerinci');
INSERT INTO `kabupaten` VALUES ('1502', '15', 'Kab. Merangin');
INSERT INTO `kabupaten` VALUES ('1503', '15', 'Kab. Sarolangun');
INSERT INTO `kabupaten` VALUES ('1504', '15', 'Kab. Batang Hari');
INSERT INTO `kabupaten` VALUES ('1505', '15', 'Kab. Muaro Jambi');
INSERT INTO `kabupaten` VALUES ('1506', '15', 'Kab. Tanjung Jabung Timur');
INSERT INTO `kabupaten` VALUES ('1507', '15', 'Kab. Tanjung Jabung Barat');
INSERT INTO `kabupaten` VALUES ('1508', '15', 'Kab. Tebo');
INSERT INTO `kabupaten` VALUES ('1509', '15', 'Kab. Bungo');
INSERT INTO `kabupaten` VALUES ('1571', '15', 'Kota Jambi');
INSERT INTO `kabupaten` VALUES ('1572', '15', 'Kota Sungai Penuh');
INSERT INTO `kabupaten` VALUES ('1601', '16', 'Kab. Ogan Komering Ulu');
INSERT INTO `kabupaten` VALUES ('1602', '16', 'Kab. Ogan Komering Ilir');
INSERT INTO `kabupaten` VALUES ('1603', '16', 'Kab. Muara Enim');
INSERT INTO `kabupaten` VALUES ('1604', '16', 'Kab. Lahat');
INSERT INTO `kabupaten` VALUES ('1605', '16', 'Kab. Musi Rawas');
INSERT INTO `kabupaten` VALUES ('1606', '16', 'Kab. Musi Banyuasin');
INSERT INTO `kabupaten` VALUES ('1607', '16', 'Kab. Banyu Asin');
INSERT INTO `kabupaten` VALUES ('1608', '16', 'Kab. Ogan Komering Ulu Selatan');
INSERT INTO `kabupaten` VALUES ('1609', '16', 'Kab. Ogan Komering Ulu Timur');
INSERT INTO `kabupaten` VALUES ('1610', '16', 'Kab. Ogan Ilir');
INSERT INTO `kabupaten` VALUES ('1611', '16', 'Kab. Empat Lawang');
INSERT INTO `kabupaten` VALUES ('1671', '16', 'Kota Palembang');
INSERT INTO `kabupaten` VALUES ('1672', '16', 'Kota Prabumulih');
INSERT INTO `kabupaten` VALUES ('1673', '16', 'Kota Pagar Alam');
INSERT INTO `kabupaten` VALUES ('1674', '16', 'Kota Lubuklinggau');
INSERT INTO `kabupaten` VALUES ('1701', '17', 'Kab. Bengkulu Selatan');
INSERT INTO `kabupaten` VALUES ('1702', '17', 'Kab. Rejang Lebong');
INSERT INTO `kabupaten` VALUES ('1703', '17', 'Kab. Bengkulu Utara');
INSERT INTO `kabupaten` VALUES ('1704', '17', 'Kab. Kaur');
INSERT INTO `kabupaten` VALUES ('1705', '17', 'Kab. Seluma');
INSERT INTO `kabupaten` VALUES ('1706', '17', 'Kab. Mukomuko');
INSERT INTO `kabupaten` VALUES ('1707', '17', 'Kab. Lebong');
INSERT INTO `kabupaten` VALUES ('1708', '17', 'Kab. Kepahiang');
INSERT INTO `kabupaten` VALUES ('1709', '17', 'Kab. Bengkulu Tengah');
INSERT INTO `kabupaten` VALUES ('1771', '17', 'Kota Bengkulu');
INSERT INTO `kabupaten` VALUES ('1801', '18', 'Kab. Lampung Barat');
INSERT INTO `kabupaten` VALUES ('1802', '18', 'Kab. Tanggamus');
INSERT INTO `kabupaten` VALUES ('1803', '18', 'Kab. Lampung Selatan');
INSERT INTO `kabupaten` VALUES ('1804', '18', 'Kab. Lampung Timur');
INSERT INTO `kabupaten` VALUES ('1805', '18', 'Kab. Lampung Tengah');
INSERT INTO `kabupaten` VALUES ('1806', '18', 'Kab. Lampung Utara');
INSERT INTO `kabupaten` VALUES ('1807', '18', 'Kab. Way Kanan');
INSERT INTO `kabupaten` VALUES ('1808', '18', 'Kab. Tulangbawang');
INSERT INTO `kabupaten` VALUES ('1809', '18', 'Kab. Pesawaran');
INSERT INTO `kabupaten` VALUES ('1810', '18', 'Kab. Pringsewu');
INSERT INTO `kabupaten` VALUES ('1811', '18', 'Kab. Mesuji');
INSERT INTO `kabupaten` VALUES ('1812', '18', 'Kab. Tulang Bawang Barat');
INSERT INTO `kabupaten` VALUES ('1813', '18', 'Kab. Pesisir Barat');
INSERT INTO `kabupaten` VALUES ('1871', '18', 'Kota Bandar Lampung');
INSERT INTO `kabupaten` VALUES ('1872', '18', 'Kota Metro');
INSERT INTO `kabupaten` VALUES ('1901', '19', 'Kab. Bangka');
INSERT INTO `kabupaten` VALUES ('1902', '19', 'Kab. Belitung');
INSERT INTO `kabupaten` VALUES ('1903', '19', 'Kab. Bangka Barat');
INSERT INTO `kabupaten` VALUES ('1904', '19', 'Kab. Bangka Tengah');
INSERT INTO `kabupaten` VALUES ('1905', '19', 'Kab. Bangka Selatan');
INSERT INTO `kabupaten` VALUES ('1906', '19', 'Kab. Belitung Timur');
INSERT INTO `kabupaten` VALUES ('1971', '19', 'Kota Pangkal Pinang');
INSERT INTO `kabupaten` VALUES ('2101', '21', 'Kab. Karimun');
INSERT INTO `kabupaten` VALUES ('2102', '21', 'Kab. Bintan');
INSERT INTO `kabupaten` VALUES ('2103', '21', 'Kab. Natuna');
INSERT INTO `kabupaten` VALUES ('2104', '21', 'Kab. Lingga');
INSERT INTO `kabupaten` VALUES ('2105', '21', 'Kab. Kepulauan Anambas');
INSERT INTO `kabupaten` VALUES ('2171', '21', 'Kota B A T A M');
INSERT INTO `kabupaten` VALUES ('2172', '21', 'Kota Tanjung Pinang');
INSERT INTO `kabupaten` VALUES ('3101', '31', 'Kab. Kepulauan Seribu');
INSERT INTO `kabupaten` VALUES ('3171', '31', 'Kota Jakarta Selatan');
INSERT INTO `kabupaten` VALUES ('3172', '31', 'Kota Jakarta Timur');
INSERT INTO `kabupaten` VALUES ('3173', '31', 'Kota Jakarta Pusat');
INSERT INTO `kabupaten` VALUES ('3174', '31', 'Kota Jakarta Barat');
INSERT INTO `kabupaten` VALUES ('3175', '31', 'Kota Jakarta Utara');
INSERT INTO `kabupaten` VALUES ('3201', '32', 'Kab. Bogor');
INSERT INTO `kabupaten` VALUES ('3202', '32', 'Kab. Sukabumi');
INSERT INTO `kabupaten` VALUES ('3203', '32', 'Kab. Cianjur');
INSERT INTO `kabupaten` VALUES ('3204', '32', 'Kab. Bandung');
INSERT INTO `kabupaten` VALUES ('3205', '32', 'Kab. Garut');
INSERT INTO `kabupaten` VALUES ('3206', '32', 'Kab. Tasikmalaya');
INSERT INTO `kabupaten` VALUES ('3207', '32', 'Kab. Ciamis');
INSERT INTO `kabupaten` VALUES ('3208', '32', 'Kab. Kuningan');
INSERT INTO `kabupaten` VALUES ('3209', '32', 'Kab. Cirebon');
INSERT INTO `kabupaten` VALUES ('3210', '32', 'Kab. Majalengka');
INSERT INTO `kabupaten` VALUES ('3211', '32', 'Kab. Sumedang');
INSERT INTO `kabupaten` VALUES ('3212', '32', 'Kab. Indramayu');
INSERT INTO `kabupaten` VALUES ('3213', '32', 'Kab. Subang');
INSERT INTO `kabupaten` VALUES ('3214', '32', 'Kab. Purwakarta');
INSERT INTO `kabupaten` VALUES ('3215', '32', 'Kab. Karawang');
INSERT INTO `kabupaten` VALUES ('3216', '32', 'Kab. Bekasi');
INSERT INTO `kabupaten` VALUES ('3217', '32', 'Kab. Bandung Barat');
INSERT INTO `kabupaten` VALUES ('3218', '32', 'Kab. Pangandaran');
INSERT INTO `kabupaten` VALUES ('3271', '32', 'Kota Bogor');
INSERT INTO `kabupaten` VALUES ('3272', '32', 'Kota Sukabumi');
INSERT INTO `kabupaten` VALUES ('3273', '32', 'Kota Bandung');
INSERT INTO `kabupaten` VALUES ('3274', '32', 'Kota Cirebon');
INSERT INTO `kabupaten` VALUES ('3275', '32', 'Kota Bekasi');
INSERT INTO `kabupaten` VALUES ('3276', '32', 'Kota Depok');
INSERT INTO `kabupaten` VALUES ('3277', '32', 'Kota Cimahi');
INSERT INTO `kabupaten` VALUES ('3278', '32', 'Kota Tasikmalaya');
INSERT INTO `kabupaten` VALUES ('3279', '32', 'Kota Banjar');
INSERT INTO `kabupaten` VALUES ('3301', '33', 'Kab. Cilacap');
INSERT INTO `kabupaten` VALUES ('3302', '33', 'Kab. Banyumas');
INSERT INTO `kabupaten` VALUES ('3303', '33', 'Kab. Purbalingga');
INSERT INTO `kabupaten` VALUES ('3304', '33', 'Kab. Banjarnegara');
INSERT INTO `kabupaten` VALUES ('3305', '33', 'Kab. Kebumen');
INSERT INTO `kabupaten` VALUES ('3306', '33', 'Kab. Purworejo');
INSERT INTO `kabupaten` VALUES ('3307', '33', 'Kab. Wonosobo');
INSERT INTO `kabupaten` VALUES ('3308', '33', 'Kab. Magelang');
INSERT INTO `kabupaten` VALUES ('3309', '33', 'Kab. Boyolali');
INSERT INTO `kabupaten` VALUES ('3310', '33', 'Kab. Klaten');
INSERT INTO `kabupaten` VALUES ('3311', '33', 'Kab. Sukoharjo');
INSERT INTO `kabupaten` VALUES ('3312', '33', 'Kab. Wonogiri');
INSERT INTO `kabupaten` VALUES ('3313', '33', 'Kab. Karanganyar');
INSERT INTO `kabupaten` VALUES ('3314', '33', 'Kab. Sragen');
INSERT INTO `kabupaten` VALUES ('3315', '33', 'Kab. Grobogan');
INSERT INTO `kabupaten` VALUES ('3316', '33', 'Kab. Blora');
INSERT INTO `kabupaten` VALUES ('3317', '33', 'Kab. Rembang');
INSERT INTO `kabupaten` VALUES ('3318', '33', 'Kab. Pati');
INSERT INTO `kabupaten` VALUES ('3319', '33', 'Kab. Kudus');
INSERT INTO `kabupaten` VALUES ('3320', '33', 'Kab. Jepara');
INSERT INTO `kabupaten` VALUES ('3321', '33', 'Kab. Demak');
INSERT INTO `kabupaten` VALUES ('3322', '33', 'Kab. Semarang');
INSERT INTO `kabupaten` VALUES ('3323', '33', 'Kab. Temanggung');
INSERT INTO `kabupaten` VALUES ('3324', '33', 'Kab. Kendal');
INSERT INTO `kabupaten` VALUES ('3325', '33', 'Kab. Batang');
INSERT INTO `kabupaten` VALUES ('3326', '33', 'Kab. Pekalongan');
INSERT INTO `kabupaten` VALUES ('3327', '33', 'Kab. Pemalang');
INSERT INTO `kabupaten` VALUES ('3328', '33', 'Kab. Tegal');
INSERT INTO `kabupaten` VALUES ('3329', '33', 'Kab. Brebes');
INSERT INTO `kabupaten` VALUES ('3371', '33', 'Kota Magelang');
INSERT INTO `kabupaten` VALUES ('3372', '33', 'Kota Surakarta');
INSERT INTO `kabupaten` VALUES ('3373', '33', 'Kota Salatiga');
INSERT INTO `kabupaten` VALUES ('3374', '33', 'Kota Semarang');
INSERT INTO `kabupaten` VALUES ('3375', '33', 'Kota Pekalongan');
INSERT INTO `kabupaten` VALUES ('3376', '33', 'Kota Tegal');
INSERT INTO `kabupaten` VALUES ('3401', '34', 'Kab. Kulon Progo');
INSERT INTO `kabupaten` VALUES ('3402', '34', 'Kab. Bantul');
INSERT INTO `kabupaten` VALUES ('3403', '34', 'Kab. Gunung Kidul');
INSERT INTO `kabupaten` VALUES ('3404', '34', 'Kab. Sleman');
INSERT INTO `kabupaten` VALUES ('3471', '34', 'Kota Yogyakarta');
INSERT INTO `kabupaten` VALUES ('3501', '35', 'Kab. Pacitan');
INSERT INTO `kabupaten` VALUES ('3502', '35', 'Kab. Ponorogo');
INSERT INTO `kabupaten` VALUES ('3503', '35', 'Kab. Trenggalek');
INSERT INTO `kabupaten` VALUES ('3504', '35', 'Kab. Tulungagung');
INSERT INTO `kabupaten` VALUES ('3505', '35', 'Kab. Blitar');
INSERT INTO `kabupaten` VALUES ('3506', '35', 'Kab. Kediri');
INSERT INTO `kabupaten` VALUES ('3507', '35', 'Kab. Malang');
INSERT INTO `kabupaten` VALUES ('3508', '35', 'Kab. Lumajang');
INSERT INTO `kabupaten` VALUES ('3509', '35', 'Kab. Jember');
INSERT INTO `kabupaten` VALUES ('3510', '35', 'Kab. Banyuwangi');
INSERT INTO `kabupaten` VALUES ('3511', '35', 'Kab. Bondowoso');
INSERT INTO `kabupaten` VALUES ('3512', '35', 'Kab. Situbondo');
INSERT INTO `kabupaten` VALUES ('3513', '35', 'Kab. Probolinggo');
INSERT INTO `kabupaten` VALUES ('3514', '35', 'Kab. Pasuruan');
INSERT INTO `kabupaten` VALUES ('3515', '35', 'Kab. Sidoarjo');
INSERT INTO `kabupaten` VALUES ('3516', '35', 'Kab. Mojokerto');
INSERT INTO `kabupaten` VALUES ('3517', '35', 'Kab. Jombang');
INSERT INTO `kabupaten` VALUES ('3518', '35', 'Kab. Nganjuk');
INSERT INTO `kabupaten` VALUES ('3519', '35', 'Kab. Madiun');
INSERT INTO `kabupaten` VALUES ('3520', '35', 'Kab. Magetan');
INSERT INTO `kabupaten` VALUES ('3521', '35', 'Kab. Ngawi');
INSERT INTO `kabupaten` VALUES ('3522', '35', 'Kab. Bojonegoro');
INSERT INTO `kabupaten` VALUES ('3523', '35', 'Kab. Tuban');
INSERT INTO `kabupaten` VALUES ('3524', '35', 'Kab. Lamongan');
INSERT INTO `kabupaten` VALUES ('3525', '35', 'Kab. Gresik');
INSERT INTO `kabupaten` VALUES ('3526', '35', 'Kab. Bangkalan');
INSERT INTO `kabupaten` VALUES ('3527', '35', 'Kab. Sampang');
INSERT INTO `kabupaten` VALUES ('3528', '35', 'Kab. Pamekasan');
INSERT INTO `kabupaten` VALUES ('3529', '35', 'Kab. Sumenep');
INSERT INTO `kabupaten` VALUES ('3571', '35', 'Kota Kediri');
INSERT INTO `kabupaten` VALUES ('3572', '35', 'Kota Blitar');
INSERT INTO `kabupaten` VALUES ('3573', '35', 'Kota Malang');
INSERT INTO `kabupaten` VALUES ('3574', '35', 'Kota Probolinggo');
INSERT INTO `kabupaten` VALUES ('3575', '35', 'Kota Pasuruan');
INSERT INTO `kabupaten` VALUES ('3576', '35', 'Kota Mojokerto');
INSERT INTO `kabupaten` VALUES ('3577', '35', 'Kota Madiun');
INSERT INTO `kabupaten` VALUES ('3578', '35', 'Kota Surabaya');
INSERT INTO `kabupaten` VALUES ('3579', '35', 'Kota Batu');
INSERT INTO `kabupaten` VALUES ('3601', '36', 'Kab. Pandeglang');
INSERT INTO `kabupaten` VALUES ('3602', '36', 'Kab. Lebak');
INSERT INTO `kabupaten` VALUES ('3603', '36', 'Kab. Tangerang');
INSERT INTO `kabupaten` VALUES ('3604', '36', 'Kab. Serang');
INSERT INTO `kabupaten` VALUES ('3671', '36', 'Kota Tangerang');
INSERT INTO `kabupaten` VALUES ('3672', '36', 'Kota Cilegon');
INSERT INTO `kabupaten` VALUES ('3673', '36', 'Kota Serang');
INSERT INTO `kabupaten` VALUES ('3674', '36', 'Kota Tangerang Selatan');
INSERT INTO `kabupaten` VALUES ('5101', '51', 'Kab. Jembrana');
INSERT INTO `kabupaten` VALUES ('5102', '51', 'Kab. Tabanan');
INSERT INTO `kabupaten` VALUES ('5103', '51', 'Kab. Badung');
INSERT INTO `kabupaten` VALUES ('5104', '51', 'Kab. Gianyar');
INSERT INTO `kabupaten` VALUES ('5105', '51', 'Kab. Klungkung');
INSERT INTO `kabupaten` VALUES ('5106', '51', 'Kab. Bangli');
INSERT INTO `kabupaten` VALUES ('5107', '51', 'Kab. Karang Asem');
INSERT INTO `kabupaten` VALUES ('5108', '51', 'Kab. Buleleng');
INSERT INTO `kabupaten` VALUES ('5171', '51', 'Kota Denpasar');
INSERT INTO `kabupaten` VALUES ('5201', '52', 'Kab. Lombok Barat');
INSERT INTO `kabupaten` VALUES ('5202', '52', 'Kab. Lombok Tengah');
INSERT INTO `kabupaten` VALUES ('5203', '52', 'Kab. Lombok Timur');
INSERT INTO `kabupaten` VALUES ('5204', '52', 'Kab. Sumbawa');
INSERT INTO `kabupaten` VALUES ('5205', '52', 'Kab. Dompu');
INSERT INTO `kabupaten` VALUES ('5206', '52', 'Kab. Bima');
INSERT INTO `kabupaten` VALUES ('5207', '52', 'Kab. Sumbawa Barat');
INSERT INTO `kabupaten` VALUES ('5208', '52', 'Kab. Lombok Utara');
INSERT INTO `kabupaten` VALUES ('5271', '52', 'Kota Mataram');
INSERT INTO `kabupaten` VALUES ('5272', '52', 'Kota Bima');
INSERT INTO `kabupaten` VALUES ('5301', '53', 'Kab. Sumba Barat');
INSERT INTO `kabupaten` VALUES ('5302', '53', 'Kab. Sumba Timur');
INSERT INTO `kabupaten` VALUES ('5303', '53', 'Kab. Kupang');
INSERT INTO `kabupaten` VALUES ('5304', '53', 'Kab. Timor Tengah Selatan');
INSERT INTO `kabupaten` VALUES ('5305', '53', 'Kab. Timor Tengah Utara');
INSERT INTO `kabupaten` VALUES ('5306', '53', 'Kab. Belu');
INSERT INTO `kabupaten` VALUES ('5307', '53', 'Kab. Alor');
INSERT INTO `kabupaten` VALUES ('5308', '53', 'Kab. Lembata');
INSERT INTO `kabupaten` VALUES ('5309', '53', 'Kab. Flores Timur');
INSERT INTO `kabupaten` VALUES ('5310', '53', 'Kab. Sikka');
INSERT INTO `kabupaten` VALUES ('5311', '53', 'Kab. Ende');
INSERT INTO `kabupaten` VALUES ('5312', '53', 'Kab. Ngada');
INSERT INTO `kabupaten` VALUES ('5313', '53', 'Kab. Manggarai');
INSERT INTO `kabupaten` VALUES ('5314', '53', 'Kab. Rote Ndao');
INSERT INTO `kabupaten` VALUES ('5315', '53', 'Kab. Manggarai Barat');
INSERT INTO `kabupaten` VALUES ('5316', '53', 'Kab. Sumba Tengah');
INSERT INTO `kabupaten` VALUES ('5317', '53', 'Kab. Sumba Barat Daya');
INSERT INTO `kabupaten` VALUES ('5318', '53', 'Kab. Nagekeo');
INSERT INTO `kabupaten` VALUES ('5319', '53', 'Kab. Manggarai Timur');
INSERT INTO `kabupaten` VALUES ('5320', '53', 'Kab. Sabu Raijua');
INSERT INTO `kabupaten` VALUES ('5371', '53', 'Kota Kupang');
INSERT INTO `kabupaten` VALUES ('6101', '61', 'Kab. Sambas');
INSERT INTO `kabupaten` VALUES ('6102', '61', 'Kab. Bengkayang');
INSERT INTO `kabupaten` VALUES ('6103', '61', 'Kab. Landak');
INSERT INTO `kabupaten` VALUES ('6104', '61', 'Kab. Pontianak');
INSERT INTO `kabupaten` VALUES ('6105', '61', 'Kab. Sanggau');
INSERT INTO `kabupaten` VALUES ('6106', '61', 'Kab. Ketapang');
INSERT INTO `kabupaten` VALUES ('6107', '61', 'Kab. Sintang');
INSERT INTO `kabupaten` VALUES ('6108', '61', 'Kab. Kapuas Hulu');
INSERT INTO `kabupaten` VALUES ('6109', '61', 'Kab. Sekadau');
INSERT INTO `kabupaten` VALUES ('6110', '61', 'Kab. Melawi');
INSERT INTO `kabupaten` VALUES ('6111', '61', 'Kab. Kayong Utara');
INSERT INTO `kabupaten` VALUES ('6112', '61', 'Kab. Kubu Raya');
INSERT INTO `kabupaten` VALUES ('6171', '61', 'Kota Pontianak');
INSERT INTO `kabupaten` VALUES ('6172', '61', 'Kota Singkawang');
INSERT INTO `kabupaten` VALUES ('6201', '62', 'Kab. Kotawaringin Barat');
INSERT INTO `kabupaten` VALUES ('6202', '62', 'Kab. Kotawaringin Timur');
INSERT INTO `kabupaten` VALUES ('6203', '62', 'Kab. Kapuas');
INSERT INTO `kabupaten` VALUES ('6204', '62', 'Kab. Barito Selatan');
INSERT INTO `kabupaten` VALUES ('6205', '62', 'Kab. Barito Utara');
INSERT INTO `kabupaten` VALUES ('6206', '62', 'Kab. Sukamara');
INSERT INTO `kabupaten` VALUES ('6207', '62', 'Kab. Lamandau');
INSERT INTO `kabupaten` VALUES ('6208', '62', 'Kab. Seruyan');
INSERT INTO `kabupaten` VALUES ('6209', '62', 'Kab. Katingan');
INSERT INTO `kabupaten` VALUES ('6210', '62', 'Kab. Pulang Pisau');
INSERT INTO `kabupaten` VALUES ('6211', '62', 'Kab. Gunung Mas');
INSERT INTO `kabupaten` VALUES ('6212', '62', 'Kab. Barito Timur');
INSERT INTO `kabupaten` VALUES ('6213', '62', 'Kab. Murung Raya');
INSERT INTO `kabupaten` VALUES ('6271', '62', 'Kota Palangka Raya');
INSERT INTO `kabupaten` VALUES ('6301', '63', 'Kab. Tanah Laut');
INSERT INTO `kabupaten` VALUES ('6302', '63', 'Kab. Kota Baru');
INSERT INTO `kabupaten` VALUES ('6303', '63', 'Kab. Banjar');
INSERT INTO `kabupaten` VALUES ('6304', '63', 'Kab. Barito Kuala');
INSERT INTO `kabupaten` VALUES ('6305', '63', 'Kab. Tapin');
INSERT INTO `kabupaten` VALUES ('6306', '63', 'Kab. Hulu Sungai Selatan');
INSERT INTO `kabupaten` VALUES ('6307', '63', 'Kab. Hulu Sungai Tengah');
INSERT INTO `kabupaten` VALUES ('6308', '63', 'Kab. Hulu Sungai Utara');
INSERT INTO `kabupaten` VALUES ('6309', '63', 'Kab. Tabalong');
INSERT INTO `kabupaten` VALUES ('6310', '63', 'Kab. Tanah Bumbu');
INSERT INTO `kabupaten` VALUES ('6311', '63', 'Kab. Balangan');
INSERT INTO `kabupaten` VALUES ('6371', '63', 'Kota Banjarmasin');
INSERT INTO `kabupaten` VALUES ('6372', '63', 'Kota Banjar Baru');
INSERT INTO `kabupaten` VALUES ('6401', '64', 'Kab. Paser');
INSERT INTO `kabupaten` VALUES ('6402', '64', 'Kab. Kutai Barat');
INSERT INTO `kabupaten` VALUES ('6403', '64', 'Kab. Kutai Kartanegara');
INSERT INTO `kabupaten` VALUES ('6404', '64', 'Kab. Kutai Timur');
INSERT INTO `kabupaten` VALUES ('6405', '64', 'Kab. Berau');
INSERT INTO `kabupaten` VALUES ('6409', '64', 'Kab. Penajam Paser Utara');
INSERT INTO `kabupaten` VALUES ('6471', '64', 'Kota Balikpapan');
INSERT INTO `kabupaten` VALUES ('6472', '64', 'Kota Samarinda');
INSERT INTO `kabupaten` VALUES ('6474', '64', 'Kota Bontang');
INSERT INTO `kabupaten` VALUES ('6501', '65', 'Kab. Malinau');
INSERT INTO `kabupaten` VALUES ('6502', '65', 'Kab. Bulungan');
INSERT INTO `kabupaten` VALUES ('6503', '65', 'Kab. Tana Tidung');
INSERT INTO `kabupaten` VALUES ('6504', '65', 'Kab. Nunukan');
INSERT INTO `kabupaten` VALUES ('6571', '65', 'Kota Tarakan');
INSERT INTO `kabupaten` VALUES ('7101', '71', 'Kab. Bolaang Mongondow');
INSERT INTO `kabupaten` VALUES ('7102', '71', 'Kab. Minahasa');
INSERT INTO `kabupaten` VALUES ('7103', '71', 'Kab. Kepulauan Sangihe');
INSERT INTO `kabupaten` VALUES ('7104', '71', 'Kab. Kepulauan Talaud');
INSERT INTO `kabupaten` VALUES ('7105', '71', 'Kab. Minahasa Selatan');
INSERT INTO `kabupaten` VALUES ('7106', '71', 'Kab. Minahasa Utara');
INSERT INTO `kabupaten` VALUES ('7107', '71', 'Kab. Bolaang Mongondow Utara');
INSERT INTO `kabupaten` VALUES ('7108', '71', 'Kab. Siau Tagulandang Biaro');
INSERT INTO `kabupaten` VALUES ('7109', '71', 'Kab. Minahasa Tenggara');
INSERT INTO `kabupaten` VALUES ('7110', '71', 'Kab. Bolaang Mongondow Selatan');
INSERT INTO `kabupaten` VALUES ('7111', '71', 'Kab. Bolaang Mongondow Timur');
INSERT INTO `kabupaten` VALUES ('7171', '71', 'Kota Manado');
INSERT INTO `kabupaten` VALUES ('7172', '71', 'Kota Bitung');
INSERT INTO `kabupaten` VALUES ('7173', '71', 'Kota Tomohon');
INSERT INTO `kabupaten` VALUES ('7174', '71', 'Kota Kotamobagu');
INSERT INTO `kabupaten` VALUES ('7201', '72', 'Kab. Banggai Kepulauan');
INSERT INTO `kabupaten` VALUES ('7202', '72', 'Kab. Banggai');
INSERT INTO `kabupaten` VALUES ('7203', '72', 'Kab. Morowali');
INSERT INTO `kabupaten` VALUES ('7204', '72', 'Kab. Poso');
INSERT INTO `kabupaten` VALUES ('7205', '72', 'Kab. Donggala');
INSERT INTO `kabupaten` VALUES ('7206', '72', 'Kab. Toli-toli');
INSERT INTO `kabupaten` VALUES ('7207', '72', 'Kab. Buol');
INSERT INTO `kabupaten` VALUES ('7208', '72', 'Kab. Parigi Moutong');
INSERT INTO `kabupaten` VALUES ('7209', '72', 'Kab. Tojo Una-una');
INSERT INTO `kabupaten` VALUES ('7210', '72', 'Kab. Sigi');
INSERT INTO `kabupaten` VALUES ('7271', '72', 'Kota Palu');
INSERT INTO `kabupaten` VALUES ('7301', '73', 'Kab. Kepulauan Selayar');
INSERT INTO `kabupaten` VALUES ('7302', '73', 'Kab. Bulukumba');
INSERT INTO `kabupaten` VALUES ('7303', '73', 'Kab. Bantaeng');
INSERT INTO `kabupaten` VALUES ('7304', '73', 'Kab. Jeneponto');
INSERT INTO `kabupaten` VALUES ('7305', '73', 'Kab. Takalar');
INSERT INTO `kabupaten` VALUES ('7306', '73', 'Kab. Gowa');
INSERT INTO `kabupaten` VALUES ('7307', '73', 'Kab. Sinjai');
INSERT INTO `kabupaten` VALUES ('7308', '73', 'Kab. Maros');
INSERT INTO `kabupaten` VALUES ('7309', '73', 'Kab. Pangkajene Dan Kepulauan');
INSERT INTO `kabupaten` VALUES ('7310', '73', 'Kab. Barru');
INSERT INTO `kabupaten` VALUES ('7311', '73', 'Kab. Bone');
INSERT INTO `kabupaten` VALUES ('7312', '73', 'Kab. Soppeng');
INSERT INTO `kabupaten` VALUES ('7313', '73', 'Kab. Wajo');
INSERT INTO `kabupaten` VALUES ('7314', '73', 'Kab. Sidenreng Rappang');
INSERT INTO `kabupaten` VALUES ('7315', '73', 'Kab. Pinrang');
INSERT INTO `kabupaten` VALUES ('7316', '73', 'Kab. Enrekang');
INSERT INTO `kabupaten` VALUES ('7317', '73', 'Kab. Luwu');
INSERT INTO `kabupaten` VALUES ('7318', '73', 'Kab. Tana Toraja');
INSERT INTO `kabupaten` VALUES ('7322', '73', 'Kab. Luwu Utara');
INSERT INTO `kabupaten` VALUES ('7325', '73', 'Kab. Luwu Timur');
INSERT INTO `kabupaten` VALUES ('7326', '73', 'Kab. Toraja Utara');
INSERT INTO `kabupaten` VALUES ('7371', '73', 'Kota Makassar');
INSERT INTO `kabupaten` VALUES ('7372', '73', 'Kota Parepare');
INSERT INTO `kabupaten` VALUES ('7373', '73', 'Kota Palopo');
INSERT INTO `kabupaten` VALUES ('7401', '74', 'Kab. Buton');
INSERT INTO `kabupaten` VALUES ('7402', '74', 'Kab. Muna');
INSERT INTO `kabupaten` VALUES ('7403', '74', 'Kab. Konawe');
INSERT INTO `kabupaten` VALUES ('7404', '74', 'Kab. Kolaka');
INSERT INTO `kabupaten` VALUES ('7405', '74', 'Kab. Konawe Selatan');
INSERT INTO `kabupaten` VALUES ('7406', '74', 'Kab. Bombana');
INSERT INTO `kabupaten` VALUES ('7407', '74', 'Kab. Wakatobi');
INSERT INTO `kabupaten` VALUES ('7408', '74', 'Kab. Kolaka Utara');
INSERT INTO `kabupaten` VALUES ('7409', '74', 'Kab. Buton Utara');
INSERT INTO `kabupaten` VALUES ('7410', '74', 'Kab. Konawe Utara');
INSERT INTO `kabupaten` VALUES ('7471', '74', 'Kota Kendari');
INSERT INTO `kabupaten` VALUES ('7472', '74', 'Kota Baubau');
INSERT INTO `kabupaten` VALUES ('7501', '75', 'Kab. Boalemo');
INSERT INTO `kabupaten` VALUES ('7502', '75', 'Kab. Gorontalo');
INSERT INTO `kabupaten` VALUES ('7503', '75', 'Kab. Pohuwato');
INSERT INTO `kabupaten` VALUES ('7504', '75', 'Kab. Bone Bolango');
INSERT INTO `kabupaten` VALUES ('7505', '75', 'Kab. Gorontalo Utara');
INSERT INTO `kabupaten` VALUES ('7571', '75', 'Kota Gorontalo');
INSERT INTO `kabupaten` VALUES ('7601', '76', 'Kab. Majene');
INSERT INTO `kabupaten` VALUES ('7602', '76', 'Kab. Polewali Mandar');
INSERT INTO `kabupaten` VALUES ('7603', '76', 'Kab. Mamasa');
INSERT INTO `kabupaten` VALUES ('7604', '76', 'Kab. Mamuju');
INSERT INTO `kabupaten` VALUES ('7605', '76', 'Kab. Mamuju Utara');
INSERT INTO `kabupaten` VALUES ('8101', '81', 'Kab. Maluku Tenggara Barat');
INSERT INTO `kabupaten` VALUES ('8102', '81', 'Kab. Maluku Tenggara');
INSERT INTO `kabupaten` VALUES ('8103', '81', 'Kab. Maluku Tengah');
INSERT INTO `kabupaten` VALUES ('8104', '81', 'Kab. Buru');
INSERT INTO `kabupaten` VALUES ('8105', '81', 'Kab. Kepulauan Aru');
INSERT INTO `kabupaten` VALUES ('8106', '81', 'Kab. Seram Bagian Barat');
INSERT INTO `kabupaten` VALUES ('8107', '81', 'Kab. Seram Bagian Timur');
INSERT INTO `kabupaten` VALUES ('8108', '81', 'Kab. Maluku Barat Daya');
INSERT INTO `kabupaten` VALUES ('8109', '81', 'Kab. Buru Selatan');
INSERT INTO `kabupaten` VALUES ('8171', '81', 'Kota Ambon');
INSERT INTO `kabupaten` VALUES ('8172', '81', 'Kota Tual');
INSERT INTO `kabupaten` VALUES ('8201', '82', 'Kab. Halmahera Barat');
INSERT INTO `kabupaten` VALUES ('8202', '82', 'Kab. Halmahera Tengah');
INSERT INTO `kabupaten` VALUES ('8203', '82', 'Kab. Kepulauan Sula');
INSERT INTO `kabupaten` VALUES ('8204', '82', 'Kab. Halmahera Selatan');
INSERT INTO `kabupaten` VALUES ('8205', '82', 'Kab. Halmahera Utara');
INSERT INTO `kabupaten` VALUES ('8206', '82', 'Kab. Halmahera Timur');
INSERT INTO `kabupaten` VALUES ('8207', '82', 'Kab. Pulau Morotai');
INSERT INTO `kabupaten` VALUES ('8271', '82', 'Kota Ternate');
INSERT INTO `kabupaten` VALUES ('8272', '82', 'Kota Tidore Kepulauan');
INSERT INTO `kabupaten` VALUES ('9101', '91', 'Kab. Fakfak');
INSERT INTO `kabupaten` VALUES ('9102', '91', 'Kab. Kaimana');
INSERT INTO `kabupaten` VALUES ('9103', '91', 'Kab. Teluk Wondama');
INSERT INTO `kabupaten` VALUES ('9104', '91', 'Kab. Teluk Bintuni');
INSERT INTO `kabupaten` VALUES ('9105', '91', 'Kab. Manokwari');
INSERT INTO `kabupaten` VALUES ('9106', '91', 'Kab. Sorong Selatan');
INSERT INTO `kabupaten` VALUES ('9107', '91', 'Kab. Sorong');
INSERT INTO `kabupaten` VALUES ('9108', '91', 'Kab. Raja Ampat');
INSERT INTO `kabupaten` VALUES ('9109', '91', 'Kab. Tambrauw');
INSERT INTO `kabupaten` VALUES ('9110', '91', 'Kab. Maybrat');
INSERT INTO `kabupaten` VALUES ('9171', '91', 'Kota Sorong');
INSERT INTO `kabupaten` VALUES ('9401', '94', 'Kab. Merauke');
INSERT INTO `kabupaten` VALUES ('9402', '94', 'Kab. Jayawijaya');
INSERT INTO `kabupaten` VALUES ('9403', '94', 'Kab. Jayapura');
INSERT INTO `kabupaten` VALUES ('9404', '94', 'Kab. Nabire');
INSERT INTO `kabupaten` VALUES ('9408', '94', 'Kab. Kepulauan Yapen');
INSERT INTO `kabupaten` VALUES ('9409', '94', 'Kab. Biak Numfor');
INSERT INTO `kabupaten` VALUES ('9410', '94', 'Kab. Paniai');
INSERT INTO `kabupaten` VALUES ('9411', '94', 'Kab. Puncak Jaya');
INSERT INTO `kabupaten` VALUES ('9412', '94', 'Kab. Mimika');
INSERT INTO `kabupaten` VALUES ('9413', '94', 'Kab. Boven Digoel');
INSERT INTO `kabupaten` VALUES ('9414', '94', 'Kab. Mappi');
INSERT INTO `kabupaten` VALUES ('9415', '94', 'Kab. Asmat');
INSERT INTO `kabupaten` VALUES ('9416', '94', 'Kab. Yahukimo');
INSERT INTO `kabupaten` VALUES ('9417', '94', 'Kab. Pegunungan Bintang');
INSERT INTO `kabupaten` VALUES ('9418', '94', 'Kab. Tolikara');
INSERT INTO `kabupaten` VALUES ('9419', '94', 'Kab. Sarmi');
INSERT INTO `kabupaten` VALUES ('9420', '94', 'Kab. Keerom');
INSERT INTO `kabupaten` VALUES ('9426', '94', 'Kab. Waropen');
INSERT INTO `kabupaten` VALUES ('9427', '94', 'Kab. Supiori');
INSERT INTO `kabupaten` VALUES ('9428', '94', 'Kab. Mamberamo Raya');
INSERT INTO `kabupaten` VALUES ('9429', '94', 'Kab. Nduga');
INSERT INTO `kabupaten` VALUES ('9430', '94', 'Kab. Lanny Jaya');
INSERT INTO `kabupaten` VALUES ('9431', '94', 'Kab. Mamberamo Tengah');
INSERT INTO `kabupaten` VALUES ('9432', '94', 'Kab. Yalimo');
INSERT INTO `kabupaten` VALUES ('9433', '94', 'Kab. Puncak');
INSERT INTO `kabupaten` VALUES ('9434', '94', 'Kab. Dogiyai');
INSERT INTO `kabupaten` VALUES ('9435', '94', 'Kab. Intan Jaya');
INSERT INTO `kabupaten` VALUES ('9436', '94', 'Kab. Deiyai');
INSERT INTO `kabupaten` VALUES ('9471', '94', 'Kota Jayapura');

-- ----------------------------
-- Table structure for komentar
-- ----------------------------
DROP TABLE IF EXISTS `komentar`;
CREATE TABLE `komentar` (
  `id_komentar` int(11) NOT NULL AUTO_INCREMENT,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `komen` text NOT NULL,
  `publish` enum('Ya','Tidak') NOT NULL,
  `id_berita` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `id_loker` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_komentar`),
  KEY `id_berita` (`id_berita`),
  KEY `id` (`id`),
  CONSTRAINT `komentar_ibfk_1` FOREIGN KEY (`id_berita`) REFERENCES `berita` (`id_berita`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `komentar_ibfk_2` FOREIGN KEY (`id`) REFERENCES `alumni` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of komentar
-- ----------------------------
INSERT INTO `komentar` VALUES ('22', '2019-05-25 09:20:29', 'eef', 'Ya', null, '2', '7');
INSERT INTO `komentar` VALUES ('23', '2019-05-25 09:21:52', 'aem', 'Ya', '7', '2', null);
INSERT INTO `komentar` VALUES ('24', '2019-05-25 10:16:29', 'siii', 'Ya', '7', '2', null);
INSERT INTO `komentar` VALUES ('25', '2019-05-25 10:17:05', 'okoko', 'Ya', null, '2', '7');

-- ----------------------------
-- Table structure for loker
-- ----------------------------
DROP TABLE IF EXISTS `loker`;
CREATE TABLE `loker` (
  `id_loker` int(11) NOT NULL AUTO_INCREMENT,
  `judul_loker` varchar(100) NOT NULL,
  `penulis` varchar(25) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(11) NOT NULL,
  `gambar` text NOT NULL,
  PRIMARY KEY (`id_loker`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of loker
-- ----------------------------
INSERT INTO `loker` VALUES ('7', 'PT. Bina Darma', 'Admin', '<p style=\"text-align: center;\">Lowongan Kerja ini sampai</p>', '2019-04-17', '0', 'Koala.jpg');

-- ----------------------------
-- Table structure for provinsi
-- ----------------------------
DROP TABLE IF EXISTS `provinsi`;
CREATE TABLE `provinsi` (
  `id_provinsi` int(4) NOT NULL,
  `nama_provinsi` varchar(30) NOT NULL,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of provinsi
-- ----------------------------
INSERT INTO `provinsi` VALUES ('11', 'Aceh');
INSERT INTO `provinsi` VALUES ('12', 'Sumatera Utara');
INSERT INTO `provinsi` VALUES ('13', 'Sumatera Barat');
INSERT INTO `provinsi` VALUES ('14', 'Riau');
INSERT INTO `provinsi` VALUES ('15', 'Jambi');
INSERT INTO `provinsi` VALUES ('16', 'Sumatera Selatan');
INSERT INTO `provinsi` VALUES ('17', 'Bengkulu');
INSERT INTO `provinsi` VALUES ('18', 'Lampung');
INSERT INTO `provinsi` VALUES ('19', 'Kepulauan Bangka Belitung');
INSERT INTO `provinsi` VALUES ('21', 'Kepulauan Riau');
INSERT INTO `provinsi` VALUES ('31', 'Dki Jakarta');
INSERT INTO `provinsi` VALUES ('32', 'Jawa Barat');
INSERT INTO `provinsi` VALUES ('33', 'Jawa Tengah');
INSERT INTO `provinsi` VALUES ('34', 'Di Yogyakarta');
INSERT INTO `provinsi` VALUES ('35', 'Jawa Timur');
INSERT INTO `provinsi` VALUES ('36', 'Banten');
INSERT INTO `provinsi` VALUES ('51', 'Bali');
INSERT INTO `provinsi` VALUES ('52', 'Nusa Tenggara Barat');
INSERT INTO `provinsi` VALUES ('53', 'Nusa Tenggara Timur');
INSERT INTO `provinsi` VALUES ('61', 'Kalimantan Barat');
INSERT INTO `provinsi` VALUES ('62', 'Kalimantan Tengah');
INSERT INTO `provinsi` VALUES ('63', 'Kalimantan Selatan');
INSERT INTO `provinsi` VALUES ('64', 'Kalimantan Timur');
INSERT INTO `provinsi` VALUES ('65', 'Kalimantan Utara');
INSERT INTO `provinsi` VALUES ('71', 'Sulawesi Utara');
INSERT INTO `provinsi` VALUES ('72', 'Sulawesi Tengah');
INSERT INTO `provinsi` VALUES ('73', 'Sulawesi Selatan');
INSERT INTO `provinsi` VALUES ('74', 'Sulawesi Tenggara');
INSERT INTO `provinsi` VALUES ('75', 'Gorontalo');
INSERT INTO `provinsi` VALUES ('76', 'Sulawesi Barat');
INSERT INTO `provinsi` VALUES ('81', 'Maluku');
INSERT INTO `provinsi` VALUES ('82', 'Maluku Utara');
INSERT INTO `provinsi` VALUES ('91', 'Papua Barat');
INSERT INTO `provinsi` VALUES ('94', 'Papua');

-- ----------------------------
-- Table structure for testimoni
-- ----------------------------
DROP TABLE IF EXISTS `testimoni`;
CREATE TABLE `testimoni` (
  `id_testimoni` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `isi_testimoni` text NOT NULL,
  `foto` text NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `publish` enum('Ya','Tidak') NOT NULL,
  PRIMARY KEY (`id_testimoni`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of testimoni
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
